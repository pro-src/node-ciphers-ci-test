const fs = require('fs');
const logs = fs.readdirSync('.').filter(filename => filename.endsWith('.log'));

const results = {};

logs.forEach(filename => {
  const data = fs.readFileSync('./' + filename, 'utf-8');

  if (!data) {
    console.log('Failed to fetch log:', filename);
    return;
  }

  // console.log('Processing', filename, '...');

  const openssl = data.match(/openssl: '([0-9a-zA-z.]+)/)[1];
  const node = data.match(/Now using node v([\d.]+)/)[1];
  const ciphers = eval(data.match(/(\[[^[]+\]) (?:'\\n\\n\\n\\n'|\n{4})/)[1]);

  results[node] = { openssl, ciphers };
});

fs.writeFileSync('./results.json', JSON.stringify(results, null, 2), 'utf-8');

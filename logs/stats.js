require('./format')

const fs = require('fs');
const results = require('./results');
const crypto = require('crypto');

getVersions();

function getVersions() {
  const openssl = [];
  const suites = {};
  const diff = {};

  Object.keys(results).forEach(version => {
    node = results[version];

    if (openssl.indexOf(node.openssl) === -1) {
      suites[node.openssl] = {
        node: [ version ],
        ciphers: node.ciphers
      };

      openssl.push(node.openssl);
    }
    else {
      let listA = suites[node.openssl].ciphers;
      let listB = node.ciphers;

      if (!Array.isArray(listA) || !Array.isArray(listB)) {
        diff[version] = node;
        // console.log(listA, node, version, 'not arrays');
      }

      if (listA.length !== listB.length) {
        diff[version] = node;
        // console.log(listA, node, version, 'not same length');
      }

      for (let i = 0; i < listA.length; i++) {
        if (listA[i] !== listB[i]) {
          diff[version] = node;
          // console.log(listA, node, version, 'mismatch at index', i);
        }
      }

      if (suites[node.openssl].node.indexOf(version) === -1) {
        suites[node.openssl].node.push(version);
      }
    }
  });

  console.log(openssl);
  console.log('There are', openssl.length, 'unique versions of openssl.');

  fs.writeFileSync('./openssl-versions.json', JSON.stringify(openssl, null, 2), 'utf-8');
  fs.writeFileSync('./suites.json', JSON.stringify(suites, null, 2), 'utf-8');
  fs.writeFileSync('./diff.json', JSON.stringify(diff, null, 2), 'utf-8');

  const hashes = [];
  const unique = [];

  Object.keys(suites).forEach(version => {
    const shasum = crypto.createHash('sha1');

    const ciphers = JSON.stringify(suites[version].ciphers);
    shasum.update(ciphers);

    const id = shasum.digest('hex');

    if (hashes.indexOf(id) === -1) {
      console.log(version, suites[version].node, suites[version].ciphers);
      hashes.push(id);
      unique.push(suites[version].ciphers);
    }
  });

  Object.keys(diff).forEach(version => {
    const shasum = crypto.createHash('sha1');

    const ciphers = JSON.stringify(diff[version].ciphers);
    shasum.update(ciphers);

    const id = shasum.digest('hex');

    if (hashes.indexOf(id) === -1) {
      hashes.push(id);
      unique.push(diff[version].ciphers);
    }
  });

  console.log('There are', hashes.length, 'unique cipher suites.');
  console.log(unique[0].filter(h => unique[1].includes(h) && unique[2].includes(h) && unique[3].includes(h)));
}


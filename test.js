#!/usr/bin/env node

var expected = [
  'AES128-GCM-SHA256',
  'AES128-SHA',
  'AES128-SHA256',
  'AES256-GCM-SHA384',
  'AES256-SHA',
  'AES256-SHA256',
  'DHE-PSK-AES128-CBC-SHA',
  'DHE-PSK-AES128-CBC-SHA256',
  'DHE-PSK-AES128-GCM-SHA256',
  'DHE-PSK-AES256-CBC-SHA',
  'DHE-PSK-AES256-CBC-SHA384',
  'DHE-PSK-AES256-GCM-SHA384',
  'DHE-PSK-CHACHA20-POLY1305',
  'DHE-RSA-AES128-GCM-SHA256',
  'DHE-RSA-AES128-SHA',
  'DHE-RSA-AES128-SHA256',
  'DHE-RSA-AES256-GCM-SHA384',
  'DHE-RSA-AES256-SHA',
  'DHE-RSA-AES256-SHA256',
  'DHE-RSA-CHACHA20-POLY1305',
  'ECDHE-ECDSA-AES128-GCM-SHA256',
  'ECDHE-ECDSA-AES128-SHA',
  'ECDHE-ECDSA-AES128-SHA256',
  'ECDHE-ECDSA-AES256-GCM-SHA384',
  'ECDHE-ECDSA-AES256-SHA',
  'ECDHE-ECDSA-AES256-SHA384',
  'ECDHE-ECDSA-CHACHA20-POLY1305',
  'ECDHE-PSK-AES128-CBC-SHA',
  'ECDHE-PSK-AES128-CBC-SHA256',
  'ECDHE-PSK-AES256-CBC-SHA',
  'ECDHE-PSK-AES256-CBC-SHA384',
  'ECDHE-PSK-CHACHA20-POLY1305',
  'ECDHE-RSA-AES128-GCM-SHA256',
  'ECDHE-RSA-AES128-SHA',
  'ECDHE-RSA-AES128-SHA256',
  'ECDHE-RSA-AES256-GCM-SHA384',
  'ECDHE-RSA-AES256-SHA',
  'ECDHE-RSA-AES256-SHA384',
  'ECDHE-RSA-CHACHA20-POLY1305',
  'PSK-AES128-CBC-SHA',
  'PSK-AES128-CBC-SHA256',
  'PSK-AES128-GCM-SHA256',
  'PSK-AES256-CBC-SHA',
  'PSK-AES256-CBC-SHA384',
  'PSK-AES256-GCM-SHA384',
  'PSK-CHACHA20-POLY1305',
  'RSA-PSK-AES128-CBC-SHA',
  'RSA-PSK-AES128-CBC-SHA256',
  'RSA-PSK-AES128-GCM-SHA256',
  'RSA-PSK-AES256-CBC-SHA',
  'RSA-PSK-AES256-CBC-SHA384',
  'RSA-PSK-AES256-GCM-SHA384',
  'RSA-PSK-CHACHA20-POLY1305',
  'SRP-AES-128-CBC-SHA',
  'SRP-AES-256-CBC-SHA',
  'SRP-RSA-AES-128-CBC-SHA',
  'SRP-RSA-AES-256-CBC-SHA',
  'TLS_AES_128_GCM_SHA256',
  'TLS_AES_256_GCM_SHA384',
  'TLS_CHACHA20_POLY1305_SHA256'
];

var i, passing = true;

try {
  var actual = require('tls').getCiphers().map(s => s.toUpperCase());
  console.log(actual, '\n'.repeat(4));

  for (i = 0; i < actual.length; i++) {
    if (actual[i] !== expected[i]) {
      passing = false;
      console.log(actual[i], 'doesn\'t match', expected[i]);
    }
  }

  console.log('\n'.repeat(4));

  for (i = 0; i < expected.length; i++) {
    if (actual.indexOf(expected[i]) === -1) {
      passing = false;
      console.log('Missing: ', expected[i]);
    }
  }
} catch (error) {
  console.log(error);
}

test('encrypted.google.com', function() {
  test('pro-src.com', function() {
    if (!passing) {
      process.exit(1);
    }
  })
});


function test(domain, cb) {
  console.log('\n'.repeat(4));

  var https = require('https');

  var options = {
    hostname: domain,
    port: 443,
    path: '/',
    method: 'GET',
    ciphers: expected.join(':')
  };

  var req = https.request(options, function (res) {
    console.log('statusCode:', res.statusCode);
    console.log('headers:', res.headers);

    cb();
  });

  req.on('error', function (e) {
    passing = false;
  });

  req.end();
}
